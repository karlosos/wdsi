package dzialowski.si.slidingpuzzles;

import sac.State;
import sac.StateFunction;

public class HMisplacedTiles extends StateFunction{
	@Override
	public double calculate(State state) {
		// liczba pól nie na swoim miejscu (z pominieciem pola ‘0’ w zliczaniu).
		SlidingPuzzle sp = (SlidingPuzzle) state;
		double misplaced_tiles = 0;
		int k = 0;
		for (int i=0; i<SlidingPuzzle.n; i++) {
			for (int j=0; j<SlidingPuzzle.n; j++) {
				if (sp.board[i][j] == 0) {
					k++;
					continue;
				}	
				if (sp.board[i][j] != k++) {
					misplaced_tiles++;
				}
			}
		}
		return misplaced_tiles;
	}

}
