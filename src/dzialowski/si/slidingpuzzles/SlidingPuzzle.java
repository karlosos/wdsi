package dzialowski.si.slidingpuzzles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import sac.StateFunction;
import sac.graph.AStar;
import sac.graph.BestFirstSearch;
import sac.graph.GraphSearchAlgorithm;
import sac.graph.GraphSearchConfigurator;
import sac.graph.GraphState;
import sac.graph.GraphStateImpl;

public class SlidingPuzzle extends GraphStateImpl {
	public static int n = 3;
	public int[][] board = null;
	
	public static final int moveLeft = 0;
	public static final int moveUp = 1;
	public static final int moveRight = 2;
	public static final int moveDown = 3;
	public static final String[] move_names = {"LEFT", "UP", "RIGHT", "DOWN"};
	
	public static Random rand = new Random();
	
	public static void main(String[] args) {
		// Podstawowy test heurystyk
		testSingle(new HManhattan());
		testSingle(new HMisplacedTiles());
		
		// Porowannie heurystyk dla 100 testow
		System.out.println("Misplaced tiles:");
		testHeuristic(new HMisplacedTiles(), 100);
		System.out.println();
		System.out.println("Manhattan:");
		testHeuristic(new HManhattan(), 100);
	}
	
	
	static public void testHeuristic(StateFunction heuristic, int number_of_tests) {
		rand = new Random(1337);
		int time = 0;
		int closed = 0;
		int open = 0;
		int path_length = 0;
		for (int i=0; i<number_of_tests; i++) {
			SlidingPuzzle sp = new SlidingPuzzle();
			sp.mixBoard(1000);
			SlidingPuzzle.setHFunction(heuristic);
			GraphSearchConfigurator conf = new GraphSearchConfigurator();
			GraphSearchAlgorithm b = new AStar(sp, conf);
			b.execute();
			time += b.getDurationTime();
			closed += b.getClosedStatesCount();
			open += b.getOpenSet().size();
			List<GraphState> sols = b.getSolutions();
			path_length += sols.get(0).getMovesAlongPath().size();
		}
		System.out.println("Time: " + (float)time/number_of_tests);
		System.out.println("Closed: " + (float)closed/number_of_tests);
		System.out.println("Open: " + (float)open/number_of_tests);
		System.out.println("Path length: " + (float)path_length/number_of_tests);
	}
	
	/**
	 * Constructor
	 */
	public SlidingPuzzle() {
		board = new int[n][n];
		int k = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				board[i][j] = k++;
			}
		}
	}
	
	/**
	 * Copy constructor
	 * @param parent
	 */
	public SlidingPuzzle(SlidingPuzzle parent) {
		board = new int[n][n];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				board[i][j] = parent.board[i][j];
	}
	
	/**
	 * Scramble board with given number of moves
	 * @param numberOfMixMoves
	 */
	public void mixBoard(int numberOfMixMoves) {
		int moves = 0;
		while (moves < numberOfMixMoves) {
			int next_move = rand.nextInt(4);
			int[] zero_pos = findZero();
			if (makeMove(zero_pos, next_move))
				moves++;
		}
	}
	
	/**
	 * Find position of zero element
	 * @return
	 */
	public int[] findZero() {
		int[] zero_pos = new int[2];
		for (int i=0; i<n; i++) {
			for (int j=0; j<n; j++) {
				if (board[i][j] == 0) {
					zero_pos[0] = i;
					zero_pos[1] = j;
					return zero_pos;
				}
			}
		}
		return zero_pos;
	}
	
	/**
	 * Check if given move is valid
	 * 
	 * @param zero_pos
	 * @param next_move
	 * @return
	 */
	public boolean isValidMove(int[] zero_pos, int next_move) {
		int i = zero_pos[0];
		int j = zero_pos[1];
		// invalid left/right moves
		if ((j == 0 && next_move == moveLeft) || (j == n-1 && next_move == moveRight)) {
			//System.out.println("Invalid move: " + next_move);
			return false;
		}
		else if ((i == 0 && next_move == moveUp) || (i == n-1 && next_move == moveDown)) {
			//System.out.println("Invalid move: " + next_move);
			return false;
		}
		return true;
	}
	
	/**
	 * Make move. Handles invalid moves.
	 * 
	 * @param zero_pos
	 * @param next_move
	 * @return
	 */
	public boolean makeMove(int[] zero_pos, int next_move) {
		if (isValidMove(zero_pos, next_move)) {
			int i = zero_pos[0];
			int j = zero_pos[1];
			if (next_move == moveLeft) {
				board[i][j] = board[i][j-1];
				board[i][j-1] = 0;
			}
			else if (next_move == moveRight) {
				board[i][j] = board[i][j+1];
				board[i][j+1] = 0;
			}
			else if (next_move == moveUp) {
				board[i][j] = board[i-1][j];
				board[i-1][j] = 0;
			}
			else if (next_move == moveDown) {
				board[i][j] = board[i+1][j];
				board[i+1][j] = 0;
			}
			//System.out.println("Move: " + next_move);
			return true;
		}
		return false;
	}
	
	@Override
	public List<GraphState> generateChildren() {
		List<GraphState> children = new ArrayList<GraphState>();
		for (int next_move=0; next_move<4; next_move++) {
			int[] zero_pos = findZero();
			if (isValidMove(zero_pos, next_move)) {
				SlidingPuzzle child = new SlidingPuzzle(this);
				child.makeMove(zero_pos, next_move);
				child.setMoveName(move_names[next_move]);
				children.add(child);
			}
		}
		return children;
	}

	@Override
	public boolean isSolution() {
		int k = 0;
		for (int i = 0; i<n; i++) {
			for (int j=0; j<n; j++) {
				if(board[i][j] != k++) 
					return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		int[] copy = new int[n*n];
		int k = 0;
		for (int i=0;i<n; i++)
			for (int j=0; j<n; j++)
				copy[k++] = board[i][j];
		return Arrays.hashCode(copy);
	}

	@Override
	public String toString() {
		StringBuilder txt = new StringBuilder();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				txt.append(board[i][j]);
				txt.append("\t");
			}
			txt.append("\n");
		}
		return txt.toString();
	}
	
	/**
	 * Function for testing. Debug only
	 * @param heuristic
	 */
	static public void testSingle(StateFunction heuristic) {
		rand = new Random(1337);
		SlidingPuzzle sp = new SlidingPuzzle();
		sp.mixBoard(1000);
		SlidingPuzzle.setHFunction(heuristic);
		System.out.println("h(s) = " + heuristic.calculate(sp));
		GraphSearchConfigurator conf = new GraphSearchConfigurator();
		GraphSearchAlgorithm b = new AStar(sp, conf);
		b.execute();
		List<GraphState> sols = b.getSolutions();
		System.out.println(sp);
		System.out.println(sols.get(0).getMovesAlongPath());
		System.out.println(sols.get(0));
		System.out.println("Time: " + b.getDurationTime());
		System.out.println("Closed: " + b.getClosedStatesCount());
		System.out.println("Open: " + b.getOpenSet().size());
		System.out.println("Solutions: " + sols.size());
	}
}
