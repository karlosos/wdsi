package dzialowski.si.slidingpuzzles;

import sac.State;
import sac.StateFunction;

public class HManhattan extends StateFunction{

	@Override
	public double calculate(State state) {
		SlidingPuzzle sp = (SlidingPuzzle) state;
		double h = 0;
		for (int i=0; i<SlidingPuzzle.n; i++) {
			for (int j=0; j<SlidingPuzzle.n; j++) {
				if (sp.board[i][j] == 0)
					continue;
				h += Math.abs(i - (int)(sp.board[i][j]/SlidingPuzzle.n)) + Math.abs(j - sp.board[i][j] % SlidingPuzzle.n);
			}
		}
		
		return h;
	}

}
