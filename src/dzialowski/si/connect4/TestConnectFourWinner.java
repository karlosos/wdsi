package dzialowski.si.connect4;

import static org.junit.Assert.*;

import org.junit.Test;
 
public class TestConnectFourWinner {

	@Test
	public void testBoardFourInRowHorizontalStartMaximizing() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
	@Test
	public void testBoardFourInRowHorizontalStartMinimizing() {
		ConnectFour c4 = new ConnectFour((byte) 2);
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		System.out.println(c4);
		assertEquals(2, c4.isWin());
	}
	
	@Test
	public void testBoardFourInRowVecticalStartMaximizing() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
	
	@Test
	public void testBoardFourInRowVecticalStartMinimizing() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
	
	@Test
	public void testBoardFourInRowHorizontalStartMinimizingLeftSide() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
	
	@Test
	public void testBoardFourInRowHorizontalStartMinimizingRightSide() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(ConnectFour.board_cols);
		assertEquals(0, c4.isWin());
		c4.makeMove(ConnectFour.board_cols);
		assertEquals(0, c4.isWin());
		c4.makeMove(ConnectFour.board_cols-1);
		assertEquals(0, c4.isWin());
		c4.makeMove(ConnectFour.board_cols-1);
		assertEquals(0, c4.isWin());
		c4.makeMove(ConnectFour.board_cols-2);
		assertEquals(0, c4.isWin());
		c4.makeMove(ConnectFour.board_cols-2);
		assertEquals(0, c4.isWin());
		c4.makeMove(ConnectFour.board_cols-3);
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
	
	@Test
	public void testBoardFourInRowHorizontalStartMinimizingRightSideThreeInRow() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(ConnectFour.board_cols);
		assertEquals(0, c4.isWin());
		c4.makeMove(ConnectFour.board_cols);
		assertEquals(0, c4.isWin());
		c4.makeMove(ConnectFour.board_cols-1);
		assertEquals(0, c4.isWin());
		c4.makeMove(ConnectFour.board_cols-1);
		assertEquals(0, c4.isWin());
		c4.makeMove(ConnectFour.board_cols-2);
		assertEquals(0, c4.isWin());
		c4.makeMove(ConnectFour.board_cols-2);
		assertEquals(0, c4.isWin());
		System.out.println(c4);
		assertEquals(0, c4.isWin());
	}
	
	@Test
	public void testBoardCeiling() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		for (int i=0; i<ConnectFour.board_rows-1; i++) {
			c4.makeMove(1);
			System.out.println(c4);
			assertEquals(0, c4.isWin());
		}
		c4.makeMove(1);
		System.out.println(c4);
		int winner = 0;
		if(c4.isMaximizingTurnNow()) {
			winner = 2;
		} else {
			winner = 1;
		}
		assertEquals(winner, c4.isWin());
	}
	
	@Test
	public void testBoardCrosswiseRightLeftSide() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
	
	@Test
	public void testBoardCrosswiseRightRightSide() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		assertEquals(0, c4.isWin());
		c4.makeMove(5);
		assertEquals(0, c4.isWin());
		c4.makeMove(5);
		assertEquals(0, c4.isWin());
		c4.makeMove(6);
		assertEquals(0, c4.isWin());
		c4.makeMove(5);
		assertEquals(0, c4.isWin());
		c4.makeMove(6);
		assertEquals(0, c4.isWin());
		c4.makeMove(6);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(6);
		c4.isWin();
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
	
	@Test
	public void testBoardCrosswiseRightTopLeft() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(2);
		c4.makeMove(1);
		c4.makeMove(4);
		c4.makeMove(3);
		c4.makeMove(6);
		c4.makeMove(5);
		
		c4.makeMove(5);
		c4.makeMove(6);
		c4.makeMove(4);
		c4.makeMove(3);
		c4.makeMove(2);
		c4.makeMove(1);
		
		c4.makeMove(1);
		c4.makeMove(2);
		c4.makeMove(3);
		c4.makeMove(4);
		c4.makeMove(5);
		c4.makeMove(6);
		
		c4.makeMove(3);
		c4.makeMove(1);
		c4.makeMove(4);
		c4.makeMove(1);
		c4.makeMove(2);
		c4.makeMove(4);
		c4.makeMove(3);
		c4.makeMove(6);
		c4.makeMove(4);
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
	
	@Test
	public void testBoardCrosswiseRightTopRight() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(2);
		c4.makeMove(3);
		c4.makeMove(4);
		c4.makeMove(5);
		c4.makeMove(6);
		c4.makeMove(1);
		
		c4.makeMove(1);
		c4.makeMove(6);
		c4.makeMove(5);
		c4.makeMove(4);
		c4.makeMove(3);
		c4.makeMove(2);
		
		c4.makeMove(2);
		c4.makeMove(4);
		c4.makeMove(3);
		c4.makeMove(5);
		c4.makeMove(6);
		c4.makeMove(1);
		
		c4.makeMove(2);
		c4.makeMove(3);
		c4.makeMove(4);
		c4.makeMove(5);
		c4.makeMove(6);
		c4.makeMove(1);
		
		c4.makeMove(5);
		c4.makeMove(6);
		c4.makeMove(6);
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
	
	@Test
	public void testBoardCrosswiseLeft() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(6);
		assertEquals(0, c4.isWin());
		c4.makeMove(5);
		assertEquals(0, c4.isWin());
		c4.makeMove(5);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		System.out.println(c4);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(6);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
	
	@Test
	public void testBoardCrosswiseLeftRightSide() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(6);
		assertEquals(0, c4.isWin());
		c4.makeMove(5);
		assertEquals(0, c4.isWin());
		c4.makeMove(5);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(6);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
	
	@Test
	public void testBoardCrosswiseLeftLeftSide() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(4);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(3);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(2);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(4);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		c4.isWin();
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
	
	@Test
	public void testBoardCrosswiseLeftTopRight() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(4);
		c4.makeMove(3);
		c4.makeMove(2);
		c4.makeMove(1);
		c4.makeMove(5);
		c4.makeMove(6);
		
		c4.makeMove(3);
		c4.makeMove(6);
		c4.makeMove(1);
		c4.makeMove(4);
		c4.makeMove(2);
		c4.makeMove(5);
		
		c4.makeMove(4);
		c4.makeMove(2);
		c4.makeMove(3);
		c4.makeMove(1);
		c4.makeMove(6);
		c4.makeMove(5);
		
		c4.makeMove(2);
		c4.makeMove(4);
		c4.makeMove(1);
		c4.makeMove(4);
		c4.makeMove(3);
		c4.makeMove(1);
		c4.makeMove(2);
		c4.makeMove(5);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
	
	@Test
	public void testBoardCrosswiseLeftTopLeft() {
		ConnectFour c4 = new ConnectFour((byte) 1);
		c4.makeMove(6);
		c4.makeMove(4);
		c4.makeMove(3);
		c4.makeMove(2);
		c4.makeMove(1);
		c4.makeMove(5);
		
		c4.makeMove(6);
		c4.makeMove(1);
		c4.makeMove(2);
		c4.makeMove(3);
		c4.makeMove(4);
		c4.makeMove(5);
		
		c4.makeMove(4);
		c4.makeMove(5);
		c4.makeMove(3);
		c4.makeMove(2);
		c4.makeMove(1);
		c4.makeMove(6);
		
		c4.makeMove(5);
		c4.makeMove(4);
		c4.makeMove(3);
		c4.makeMove(2);
		c4.makeMove(1);
		c4.makeMove(6);
		
		c4.makeMove(2);
		c4.makeMove(1);
		assertEquals(0, c4.isWin());
		c4.makeMove(1);
		System.out.println(c4);
		assertEquals(1, c4.isWin());
	}
}
