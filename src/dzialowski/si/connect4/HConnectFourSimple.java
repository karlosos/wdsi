package dzialowski.si.connect4;

import sac.State;
import sac.StateFunction;

public class HConnectFourSimple extends StateFunction {

	@Override
	public double calculate(State state) {
		ConnectFour c4 = (ConnectFour) state;
		
		// sprawdzanie czy ktos wygrywa
		if (c4.isWin() == 1) {
			return(Double.POSITIVE_INFINITY);
		}
		else if (c4.isWin() == 2) {
			return(Double.NEGATIVE_INFINITY);
		}
		
		return 0;
	}

}
