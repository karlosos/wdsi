package dzialowski.si.connect4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import sac.game.AlphaBetaPruning;
import sac.game.GameSearchAlgorithm;
import sac.game.GameSearchConfigurator;
import sac.game.GameState;
import sac.game.GameStateImpl;
import sac.game.MinMax;

public class ConnectFour extends GameStateImpl {
	// stale reprezentujece rozmiar planszy
	public static final int board_rows = 6;
	public static final int board_cols = 7;

	// stale reprezentujace symbole graczy
	public static final byte empty = 0;
	public static final byte player_one = 1;
	public static final byte player_two = 2;

	// plansza gry
	byte[][] board = null;

	/**
	 * Funkcja programu
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.print("Kto zaczyna? [1/2]: ");
		Scanner scan = new Scanner(System.in);
		byte starting_player = scan.nextByte();
		// ustawienie funkcji heurystycznej oceniajacej przewage
		ConnectFour.setHFunction(new HConnectFour());
		ConnectFour c4 = new ConnectFour(starting_player);
		//c4.setHFunction(new HConnectFour());

		// zmienna na ruch gracza/ai
		int column_number;
		// wyjscie z petli gry
		boolean is_finished = false;

		// konfiguracja ai
		GameSearchConfigurator conf = new GameSearchConfigurator();
		// ustawienie na 5 ruchow (10 polruchow)
		conf.setDepthLimit(2);
		GameSearchAlgorithm alg = new AlphaBetaPruning(c4, conf);
		//GameSearchAlgorithm alg = new MinMax(c4, conf);

		// glowna petla gry
		while(!is_finished) {
			// gracz 1 - my - player_one
			if(c4.isMaximizingTurnNow()) {
				System.out.println(c4);
				System.out.print("Podaj kolumne: ");
				// wykonuje ruch podany przez gracza
				column_number = scan.nextInt();
				c4.makeMove(column_number);
				// sprawdza czy jest zwyciezca
				is_finished = c4.checkWinner();
			}
			// gracz 2  - ai - player_two
			else {
				System.out.println("Czekaj na ruch gracza...");
				alg.execute();
				// Wyswietla informacje o ruchach ai
				//System.out.println(alg.getFirstBestMove());	// pierwszy ruch 
				System.out.println(alg.getBestMoves());			// wyswietla jakie ruchy sa najlepsze
				System.out.println( "moves scores: " + alg.getMovesScores());	// wyswietla oceny ruchow

				// wykonuje ruch wybrany przez komputer
				column_number = Integer.parseInt(alg.getFirstBestMove());
				c4.makeMove(column_number);
				System.out.println("AI wybral: " + column_number);
				// sprawdza czy jest zwyciezca
				is_finished = c4.checkWinner();
			}
		}
		scan.close();
	}

	/**
	 * Funkcja sprawdzajaca czy jest zwyciezca planszy
	 * Wyswietla komunikat o wygranej/przegranej
	 * @return
	 */
	public boolean checkWinner() {
		byte winner  = isWin();
		if (winner == player_one) {
			System.out.println("Wygrales!");
			System.out.println(this);
			return true;
		}
		else if(winner == player_two) {
			System.out.println("Przegrales");
			System.out.println(this);
			return true;
		}
		return false;
	}

	/**
	 * Konstruktor domyslny
	 * 
	 * @param starting_player - gracz ktory zaczyna (player_one lub player_two)
	 */
	public ConnectFour(byte starting_player) {
		super();
		// ustawiamy kto ma zaczynac gre
		if (starting_player == player_one)
			setMaximizingTurnNow(true);
		else
			setMaximizingTurnNow(false);

		// ustawianie pustej planszy
		board = new byte[board_rows][board_cols];
		for (int i = 0; i < board_rows; i++) {
			for (int j = 0; j < board_cols; j++) {
				board[i][j] = empty;
			}
		}
	}

	/**
	 * Konstruktor kopiujacy
	 * @param parent
	 */
	public ConnectFour(ConnectFour parent) {
		board = new byte[board_rows][board_cols];
		for (int i = 0; i < board_rows; i++) {
			for (int j = 0; j < board_cols; j++) {
				board[i][j] = parent.board[i][j];
			}
		}
		// kopiowanie flagi ruchu
		this.setMaximizingTurnNow(parent.isMaximizingTurnNow());
	}

	/**
	 * Funkcja wykonujaca ruch na planszy
	 * 
	 * @param columnNumber - numer kolumny (od 1 do board_cols), INDEKSOWANY OD 1!
	 * @return
	 */
	public boolean makeMove(int columnNumber) {
		// dekrementacja bo numer kolumny indeksuje od 1
		// mialem to zmienic ale za duzo roboty
		columnNumber--;
		// sprawdza czy nie podalismy za duza wartosc
		if (columnNumber > board_cols)
			return false;
		if (board[0][columnNumber] != empty)
			return false;
		
		// sprawdzanie czy kolumna jest pusta
		if (board[board_rows-1][columnNumber] == empty) {
			// w zaleznosci kogo ruch to taki symbol wklada do planszy
			if (isMaximizingTurnNow())
				board[board_rows-1][columnNumber] = player_one;
			else
				board[board_rows-1][columnNumber] = player_two;
			// zmieniamy zeby ruch mial gracz przeciwny
			setMaximizingTurnNow(!isMaximizingTurnNow());
			return true;
		}
		
		// szukanie wolnego miejsca
		for (int i=0; i<=board_rows; i++) {
			// szukanie pierwszego wolnego miejsca w kolumnie
			if(board[i][columnNumber] != empty) {
				if (isMaximizingTurnNow())
					board[i-1][columnNumber] = player_one;
				else
					board[i-1][columnNumber] = player_two;
				// zmieniamy zeby ruch mial gracz przeciwny
				setMaximizingTurnNow(!isMaximizingTurnNow());
				return true;
			}

		}
		return true;
	}

	/**
	 * Szukanie zwyciezcy na suficie
	 * @return
	 */
	private byte checkCeiling() {
		// kto dotyka sufitu ten wygrywa
		for (int i=0; i<board_cols; i++) {
			if (board[0][i] != 0)
				return board[0][i];
		}
		return 0;
	}

	/**
	 * Szukanie 'czworki' w wierszach
	 * @return zwraca numer wygranego gracza, 0 w przypadku braku roztrzygniecia
	 */
	private byte checkRows() {
		for (int i=0; i<board_rows; i++) {
			// pierszy symbol w wierszu
			byte last = board[i][0];
			// suma (streak) jednakowych znakow w rzedzie
			byte sum = 0;
			for (int j=0; j<board_cols; j++) {
				// jezeli znak taki sam jak ostatnio to inkrementujemy licznik
				if (board[i][j] == last) {
					sum++;
				} else {
					// "zerujemy" licznik - 1 znak pod rzad
					sum = 1;
					// ustawiamy nowy ostatni znak
					last = board[i][j];
				}
				// jezeli pod rzad 4 te same znaki i nie sa to zera to zwroc wartosc
				if (sum == 4 && last != empty) {
					return last;
				}
			}
		}
		return 0;
	}

	/**
	 * Szukanie 'czworki' w kolumnach
	 * @return
	 */
	private byte checkColumns() {
		for (int i=0; i<board_cols; i++) {
			// pierwszy znak w kolumnie
			byte last = board[0][i];
			byte sum = 0;
			// dla kazdego wiersza
			for (int j=0; j<board_rows; j++) {
				// jezeli znak ten sam co poprzednio zwieksz licznik
				if (board[j][i] == last) {
					sum++;
				} else {
					// 'zeruj' licznik i ustaw ostatni znak na aktualny
					sum = 1;
					last = board[j][i];
				}
				// jezeli licznik doliczyl do 4 i sa to znaki niepuste to zwroc 
				// ten znak gracza ktory wygral
				if (sum == 4 && last != empty) {
					return last;
				}
			}
		}
		return 0;
	}

	/**
	 * Szukanie po ukosie
	 */
	private byte checkCrosswise() {
		for (int i=board_rows-1; i>2; i--) {
			for (int j=0; j<board_cols-4+1; j++) {
				byte subset = 0;
				// jezeli pierwsza komorka po skosie w prawo to 0 to nie sprawdzaj
				if (board[i][j] != empty) {
					subset = checkCrosswiseRightSubset(i, j);
					if (subset != empty)
						return(subset);
				}

				// jezeli pierwsza komorka po skosie w lewo to 0 to nie sprawdzaj
				if (board[i-3][j] != empty) {
					subset = checkCrosswiseLeftSubset(i, j);
					if (subset != empty)
						return(subset);
				}
			}
		}
		return 0;
	}

	/**
	 * Sprawdz czy wycinek 4x4 ma rozwiazanie po skosie w prawo
	 * 
	 * Szuka:
	 * 0 0 0 1
	 * 0 0 1 0 
	 * 0 1 0 0 
	 * 1 0 0 0 
	 * 
	 * @param i
	 * @param j
	 * @return
	 */
	private byte checkCrosswiseRightSubset(int i, int j) {
		byte last = board[i][j];
		for (int k=0; k<4; k++) {
			// jezeli przerwie ciaglosc to wyjdz z petli
			if (board[i-k][j+k] != last)
				break;
			// jezeli petla doszla do konca i znaki nie sa zerami
			if (k==3 && last != empty)
				return(last);
		}
		return 0;
	}

	/**
	 * Sprawdz czy wycinek 4x4 ma rozwiazanie po skosie w lewo
	 * 
	 * Szuka:
	 * 1 0 0 0
	 * 0 1 0 0 
	 * 0 0 1 0 
	 * 0 0 0 1
	 * 
	 * @param i
	 * @param j
	 * @return
	 */
	private byte checkCrosswiseLeftSubset(int i, int j) {
		// zaczynami od gory dlatego odejmuje 3 
		byte last = board[i-3][j];
		for (int k=3, l=0; k>=0; k--, l++) {
			if (board[i-k][j+l] != last)
				break;
			if (k==0 && last != empty)
				return(last);
		}
		return 0;
	}

	/**
	 * Funkcja sprawdz kto wygrywa plansze
	 *
	 * @return znak gracza wygrywajacego (empty jak remis, player_one, player_two)
	 */
	public byte isWin() {
		byte winner = empty;
		// szukanie na suficie
		winner = checkCeiling();
		if (winner != empty)
			return winner;

		// szukanie w wierszach
		winner = checkRows();
		if (winner != empty)
			return winner;

		// szukanie w kolumnach
		winner = checkColumns();
		if (winner != empty)
			return winner;

		// szukanie po ukosie
		winner = checkCrosswise();
		if (winner != empty)
			return winner;

		return empty;
	}


	@Override
	public int hashCode() {
		byte[] copy = new byte[board_rows*board_cols];
		int k = 0;
		for (int i=0;i<board_rows; i++)
			for (int j=0; j<board_cols; j++)
				copy[k++] = board[i][j];
		return Arrays.hashCode(copy);
	}

	@Override
	public String toString() {
		StringBuilder txt = new StringBuilder();

		// wyswietlanie numerow kolumn
		for (int i=1; i<board_cols+1; i++) {
			txt.append(i);
			txt.append("\t");
		}
		txt.append("\n");

		// wyswietlanie przegrody
		for (int i=1; i<board_cols+1; i++) {
			txt.append('-');
			txt.append("\t");
		}
		txt.append("\n");

		// wypisywanie planszy gry
		for (int i = 0; i < board_rows; i++) {
			for (int j = 0; j < board_cols; j++) {
				txt.append(board[i][j]);
				txt.append("\t");
			}
			txt.append("\n");
		}
		return txt.toString();
	}

	@Override
	public List<GameState> generateChildren() {
		List<GameState> children = new ArrayList<GameState>();
		for (int i=1; i<=board_cols; i++) {
			ConnectFour child = new ConnectFour(this);
			if (child.makeMove(i)) {
				children.add(child);
				child.setMoveName(Integer.toString(i));
			}
		}
		return children;
	}
}
