package dzialowski.si.connect4;

import static org.junit.Assert.*;

import org.junit.Test;

import sac.game.AlphaBetaPruning;
import sac.game.GameSearchAlgorithm;
import sac.game.GameSearchConfigurator;
import sac.game.MinMax;

public class TestConnectFourAlgorithms {

	@Test
	public void algDepthTest() {
		ConnectFour.setHFunction(new HConnectFour());
		ConnectFour c4 = new ConnectFour(ConnectFour.player_one);
		
		// player_one algorithm config
		GameSearchConfigurator conf_player_one = new GameSearchConfigurator();
		conf_player_one.setDepthLimit(5);
		GameSearchAlgorithm alg_player_one = new AlphaBetaPruning(c4, conf_player_one);
		
		// player_one algorithm config
		GameSearchConfigurator conf_player_two = new GameSearchConfigurator();
		conf_player_two.setDepthLimit(1);
		GameSearchAlgorithm alg_player_two = new MinMax(c4, conf_player_two);
		
		// zmienna na ruch gracza/ai
		int column_number;
		// wyjscie z petli gry
		boolean is_finished = false;
		
		while(!is_finished) {
			if(c4.isMaximizingTurnNow()) {
				alg_player_one.execute();
				column_number = Integer.parseInt(alg_player_one.getFirstBestMove());
				c4.makeMove(column_number);
				is_finished = c4.checkWinner();
			}
			else {
				alg_player_two.execute();
				column_number = Integer.parseInt(alg_player_two.getFirstBestMove());
				c4.makeMove(column_number);
				is_finished = c4.checkWinner();
			}
		}
		System.out.println(c4);
		assertEquals(ConnectFour.player_one, c4.isWin());
	}

}
