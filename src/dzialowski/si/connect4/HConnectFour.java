package dzialowski.si.connect4;

import sac.State;
import sac.StateFunction;

public class HConnectFour extends StateFunction {

	@Override
	public double calculate(State state) {
		ConnectFour c4 = (ConnectFour) state;
		double score = 0;
		// sprawdzanie czy ktos wygrywa
		if (c4.isWin() == 1) {
			return(Double.POSITIVE_INFINITY);
		}
		else if (c4.isWin() == 2) {
			return(Double.NEGATIVE_INFINITY);
		}
		
		// jezeli 3 pole od gory zajete to nagradzaj 
		for (int i=0; i<ConnectFour.board_cols; i++) {
			if (c4.board[2][i] != ConnectFour.empty) {
				if (c4.board[2][i] == ConnectFour.player_one)
					score += 2;
				else
					score -= 2;
			}
		}
		
		// srodkowy punktowany
		byte middle = c4.board[ConnectFour.board_rows-1][ConnectFour.board_cols/2];
		if (middle == ConnectFour.player_one)
			score += 0.5;
		else if (middle == ConnectFour.player_two)
			score -= 0.5;
		
		// srodkowy na lewo
		middle = c4.board[ConnectFour.board_rows-1][ConnectFour.board_cols/2-1];
		if (middle == ConnectFour.player_one)
			score += 0.2;
		else if (middle == ConnectFour.player_two)
			score -= 0.2;
		
		// srodkowy na prawo
		middle = c4.board[ConnectFour.board_rows-1][ConnectFour.board_cols/2+1];
		if (middle == ConnectFour.player_one)
			score += 0.2;
		else if (middle == ConnectFour.player_two)
			score -= 0.2;
		
		int streak = 0;
		// serie
		for (int i=0; i<ConnectFour.board_cols-4; i++) {
			for (int j=0; j<ConnectFour.board_rows-4; j++) {
				// wyslij sonde w pziomie
				byte last = c4.board[i][j];
				streak = 0;
				
				// streak w poziomie
				for (int k=0; k<4; k++) {
					if (c4.board[i][j+k] != last && c4.board[i][j+k] != ConnectFour.empty) {
						streak = 0;
						break;
					}
					if (c4.board[i][j+k] != ConnectFour.empty) {
						last = c4.board[i][j+k];
						streak++;
					}
				}
				if (streak == 2) {
					if (last == ConnectFour.player_one)
						score += 2;
					else
						score -= 2;
				} else if (streak == 3) {
					if (last == ConnectFour.player_one)
						score += 4;
					else
						score -= 4;
				}
				
				// wyslij sonde w pziomie
				last = c4.board[i][j];
				streak = 0;
				
				// streak w pionie
				for (int k=0; k<4; k++) {
					if (c4.board[i+k][j] != last && c4.board[i+k][j] != ConnectFour.empty) {
						streak = 0;
						break;
					}
					if (c4.board[i+k][j] != ConnectFour.empty) {
						last = c4.board[i+k][j];
						streak++;
					}
				}
				if (streak == 2) {
					if (last == ConnectFour.player_one)
						score += 1;
					else
						score -= 1;
				} else if (streak == 3) {
					if (last == ConnectFour.player_one)
						score += 3;
					else
						score -= 3;
				}
				
				// wyslij sonde w ukos prawo
				last = c4.board[i][j];
				streak = 0;
				
				// streak w ukosie
				for (int k=0; k<4; k++) {
					if (c4.board[i+k][j+k] != last && c4.board[i+k][j+k] != ConnectFour.empty) {
						streak = 0;
						break;
					}
					if (c4.board[i+k][j+k] != ConnectFour.empty) {
						last = c4.board[i+k][j+k];
						streak++;
					}
				}
				if (streak == 2) {
					if (last == ConnectFour.player_one)
						score += 2;
					else
						score -= 2;
				} else if (streak == 3) {
					if (last == ConnectFour.player_one)
						score += 4;
					else
						score -= 4;
				}
				
				// wyslij sonde w ukos lewo
				last = c4.board[i][j];
				streak = 0;
				
				// streak w ukosie
				for (int k=0; k<4; k++) {
					if (c4.board[i+k][j+k] != last && c4.board[i+k][j+k] != ConnectFour.empty) {
						streak = 0;
						break;
					}
					if (c4.board[i+k][j+k] != ConnectFour.empty) {
						last = c4.board[i+k][j+k];
						streak++;
					}
				}
				if (streak == 2) {
					if (last == ConnectFour.player_one)
						score += 2;
					else
						score -= 2;
				} else if (streak == 3) {
					if (last == ConnectFour.player_one)
						score += 4;
					else
						score -= 4;
				}
				
				// wyslij sonde w ukos prawo
				last = c4.board[i][j];
				streak = 0;
				
				// streak w ukosie
				for (int k=0, l=3; k<4; k++, l--) {
					if (c4.board[i+l][j+k] != last && c4.board[i+l][j+k] != ConnectFour.empty) {
						streak = 0;
						break;
					}
					if (c4.board[i+l][j+k] != ConnectFour.empty) {
						last = c4.board[i+l][j+k];
						streak++;
					}
				}
				if (streak == 2) {
					if (last == ConnectFour.player_one)
						score += 2;
					else
						score -= 2;
				} else if (streak == 3) {
					if (last == ConnectFour.player_one)
						score += 4;
					else
						score -= 4;
				}
			}
		}
		
		return score;
	}

}
