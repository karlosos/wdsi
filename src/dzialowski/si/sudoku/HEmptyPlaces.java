package dzialowski.si.sudoku;

import sac.State;
import sac.StateFunction;

public class HEmptyPlaces extends StateFunction {
	@Override
	public double calculate(State state) {
		Sudoku sudoku = (Sudoku) state;
		return sudoku.getZeros();
	}
}
