package dzialowski.si.sudoku;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import sac.graph.BestFirstSearch;
import sac.graph.GraphSearchAlgorithm;
import sac.graph.GraphSearchConfigurator;
import sac.graph.GraphState;
import sac.graph.GraphStateImpl;

public class Sudoku extends GraphStateImpl {
	public static final int n = 3;
	public static final int n2 = n * n;

	private byte[][] board = null;
	private int zeros = n2 * n2;

	public static void main(String[] args) {
		//String sudokutxt = "003020600900305001001806400008102900700000008006708200002609500800203009005010300";
		String sudokutxt = "003020000900300001001800400008102900700000000006008200002600500800203009005010000";
		Sudoku s = new Sudoku();
		s.frontStringn3(sudokutxt);
		System.out.println(s);
		System.out.println(s.zeros);
		System.out.println(s.isLegal());

		Sudoku.setHFunction(new HEmptyPlaces());
		GraphSearchConfigurator conf = new GraphSearchConfigurator();
		conf.setWantedNumberOfSolutions(Integer.MAX_VALUE);
		GraphSearchAlgorithm b = new BestFirstSearch(s, conf);
		b.execute();
		List<GraphState> sols = b.getSolutions();
		for (GraphState sol : sols) {
			System.out.println(sol);
			System.out.println("========");

		}
		System.out.println("Time: " + b.getDurationTime());
		System.out.println("Closed: " + b.getClosedStatesCount());
		System.out.println("Open: " + b.getOpenSet().size());
		System.out.println("Solutions: " + sols.size());
	}

	public Sudoku() {
		board = new byte[n2][n2];
		for (int i = 0; i < n2; i++) {
			for (int j = 0; j < n2; j++) {
				board[i][j] = 0;
			}
		}
	}

	public Sudoku(Sudoku parent) {
		board = new byte[n2][n2];
		for (int i = 0; i < n2; i++)
			for (int j = 0; j < n2; j++) {
				board[i][j] = parent.board[i][j];
			}
		zeros = parent.zeros;
	}

	@Override
	public String toString() {
		StringBuilder txt = new StringBuilder();
		for (int i = 0; i < n2; i++) {
			for (int j = 0; j < n2; j++) {
				txt.append(board[i][j]);
				if ((j + 1) % n == 0) {
					txt.append("\t |");
				}
				txt.append("\t");
			}
			if ((i + 1) % n == 0) {
				txt.append("\n");
			}
			txt.append("\n");
		}
		return txt.toString();
	}
	
	public void frontStringn3(String txt) {
		int k = 0;
		for (int i = 0; i < n2; i++) {
			for (int j = 0; j < n2; j++) {
				board[i][j] = Byte.valueOf(txt.substring(k, k + 1));
				k++;
			}
		}
		refreshZeros();
	}

	// czy plansza jest legalna
	public boolean isLegal() {
		byte[] group = new byte[n2];
		for (int i = 0; i < n2; i++) {
			// wiersze
			for (int j = 0; j < n2; j++) {
				group[j] = board[i][j];
			}
			if (!isGroupLegal(group))
				return false;
			// kolumny
			for (int j = 0; j < n2; j++) {
				group[j] = board[j][i];
			}
			if (!isGroupLegal(group))
				return false;
		}

		// kwadraty
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				int m = 0;
				for (int k = 0; k < n; k++) {
					for (int l = 0; l < n; l++) {
						group[m++] = board[i * n + k][j * n + l];
					}
				}
				if (!isGroupLegal(group))
					return false;
			}
		}
		return true;
	}

	private boolean isGroupLegal(byte[] group) {
		boolean[] visited = new boolean[n2];
		for (int i = 0; i < n2; i++) {
			visited[i] = false;
		}

		for (int i = 0; i < n2; i++) {
			if (group[i] == 0)
				continue;

			if (visited[group[i] - 1])
				return false;
			else
				visited[group[i] - 1] = true;
		}

		return true;
	}

	private void refreshZeros() {
		zeros = 0;
		for (int i = 0; i < n2; i++)
			for (int j = 0; j < n2; j++)
				if (board[i][j] == 0)
					zeros++;
	}

	@Override
	public List<GraphState> generateChildren() {
		List<GraphState> children = new ArrayList<GraphState>();
		int i = 0, j = 0;
		zeroFound: for (i = 0; i < n2; i++)
			for (j = 0; j < n2; j++)
				if (board[i][j] == 0)
					break zeroFound;

		if (i == n2)
			return children;

		for (int k = 0; k < n2; k++) {
			Sudoku child = new Sudoku(this);
			child.board[i][j] = (byte) (k + 1);
			if (child.isLegal()) {
				children.add(child);
				child.zeros--;
			}
		}

		return children;
	}

	@Override
	public boolean isSolution() {
		return (zeros == 0 && isLegal());
	}

	public int getZeros() {
		return zeros;
	}

	@Override
	public int hashCode() {
		//return toString().hashCode();
		byte[] copy = new byte[n2*n2];
		int k = 0;
		for (int i=0;i<n2; i++)
			for (int j=0; j<n2; j++)
				copy[k++] = board[i][j];
		return Arrays.hashCode(copy);
	}
}
