package dzialowski.si.sudoku;

import java.util.Arrays;

public class HelloWorld {
	public static void main(String[] args) {
		// Zabawa z hashCode()
		int[] o1 = new int[] {3, 7, 8};
		int[] o2 = new int[] {3, 7, 8};
		// hashcode z tablicy
		System.out.println(Arrays.hashCode(o1));
		System.out.println(Arrays.hashCode(o2));
	}
}